package com.cifop.school_mngEarApp.rest;

import java.util.List;

import com.cifop.school_mngEarApp.metiers.ClassRoomService;
import com.cifop.school_mngEarApp.entities.*;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path(value = "/classrooms")
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class ClassroomRestApi {

	@Inject
	ClassRoomService classRoomService;

	@GET
	public List<ClassRoom> listClassRooms() {
		return classRoomService.findAllClassRoom();
	}

	@GET
	@Path("/{id}")
	public ClassRoom findClassRoomById(@PathParam("id") long id) {
		return classRoomService.findClassRoomById(id);
	}

	@POST
	public ClassRoom saveClassRoom(ClassRoom classroom) {
		classRoomService.saveClassroom(classroom);
		return classroom;
	}

	@PUT
	@Path("/{id}")
	public ClassRoom updateClassRoom(@PathParam("id") long id, ClassRoom newClassRoom) {

		classRoomService.updateClassRoomWithId(id, newClassRoom);
		return newClassRoom;
	}

}
