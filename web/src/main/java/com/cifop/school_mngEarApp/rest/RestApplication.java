package com.cifop.school_mngEarApp.rest;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath(value = "/api")
public class RestApplication extends Application {
	

}
