package com.cifop.school_mngEarApp.controller;

import com.cifop.school_mngEarApp.entities.*;
import com.cifop.school_mngEarApp.metiers.ClassRoomService;

import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
@RequestScoped
public class ClassroomController {

	@Inject
	ClassRoomService classRoomService;
	@Inject
	FacesContext facesContext;
	
	ClassRoom classRoom = new ClassRoom();

	public void doCreateClassRoom() {
		classRoomService.saveClassroom(classRoom);
		facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                "Class is saved successfully with id " + classRoom.getId()));
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

}
