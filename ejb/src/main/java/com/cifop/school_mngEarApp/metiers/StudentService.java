package com.cifop.school_mngEarApp.metiers;

import jakarta.ejb.Stateful;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.List;

import com.cifop.school_mngEarApp.entities.*;

@Stateful
public class StudentService {

	@PersistenceContext
	private EntityManager entityManager;

	public void saveStudent(Student student) {
		entityManager.persist(student);	
	}

	public void deleteStudent(Student student) {
		entityManager.remove(student);
	}

	public Student findStudentById(Long id) {
		return entityManager.find(Student.class, id);
	}
	public Student updateStudent(Student newStudent) {
		return entityManager.merge(newStudent);
		
	}
	//jakarta ne posséde pas findAll=> createQuery  : requete JPQL
	public List<Student> listStudentCreateQuery(){
		return  entityManager.createQuery("SELECT s from Student s").getResultList();
		
	}
	//CreateNamedQuery on la met dans l'entité :  nativeQuery :requete JPQL
	public List<Student> listStudentCreateNamedQuery(){
		return  entityManager.createNamedQuery("FindAllStudents").getResultList();
		
	}
	//CreateNativeQuery  :requete JPQL : on ecrit une requete sql tt simplement
		public List<Student> listStudentCreateNativeQuery(){
			return  entityManager.createNativeQuery("SELECT * FROM Student ").getResultList();
			
		}
		
		//Exercice Student find a list of student by firstname Createquery
		public List<Student> FindStudentByFirstNameCreateQuery(String firsName){
			return  entityManager.createQuery("SELECT s from Student s where s.firstName= :firstName "+firsName).getResultList();
			
		}
		//CreateNamedQuery on la met dans l'entité :  nativeQuery :requete JPQL
		public List<Student> FindStudentByFirstNameQuery(String firsName){
			return  entityManager.createNamedQuery("FindStudentByFirstNameQuery")
					.setParameter("firstName", firsName)
					.getResultList();	
		}
		
		
}
