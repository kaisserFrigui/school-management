package com.cifop.school_mngEarApp.metiers.soap;

import java.util.List;

import com.cifop.school_mngEarApp.entities.Student;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;

@WebService
public interface StudentSoapWebService {
	@WebMethod
	public Student findStudentById(@WebParam(name = "id",targetNamespace="http://soap.metiers.school_mngEarApp.cifop.com/")long id);
	@WebMethod
	public List<Student> findAllStudents();

}
