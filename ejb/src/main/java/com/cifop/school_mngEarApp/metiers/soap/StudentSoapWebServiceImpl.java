package com.cifop.school_mngEarApp.metiers.soap;

import com.cifop.school_mngEarApp.metiers.StudentService;

import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.jws.WebService;

import java.util.List;

import com.cifop.school_mngEarApp.entities.*;

@Stateless
@WebService
public class StudentSoapWebServiceImpl implements StudentSoapWebService {
	// @Inject ou @EJB
	@EJB
	StudentService studentService;

	@Override
	public Student findStudentById(long id) {
		return studentService.findStudentById(id);
	}

	@Override
	public List<Student> findAllStudents() {
		return studentService.listStudentCreateQuery();
	}

}
