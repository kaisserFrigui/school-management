package com.cifop.school_mngEarApp.metiers;

import java.util.List;

import com.cifop.school_mngEarApp.entities.ClassRoom;

import jakarta.ejb.Stateful;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateful
public class ClassRoomService {
	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public void saveClassroom(ClassRoom classRoom) {
		entityManager.persist(classRoom);	
	}
	
	public void updateClassRoom(ClassRoom classRoom) {
		entityManager.merge(classRoom);
	}

	/**
	 * 
	 * @param classRoom
	 */
	public void removeClassRoom(ClassRoom classRoom) {
		entityManager.remove(classRoom);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public ClassRoom findClassRoomById(long id) {
		return entityManager.find(ClassRoom.class, id);
	}

	public List<ClassRoom> findAllClassRoom() {
		return  entityManager
				.createQuery("select c from ClassRoom c")
				.getResultList();
	}
	
	public void updateClassRoomWithId(long id,ClassRoom classRoomToUpdate)
	{
		ClassRoom classRoom=findClassRoomById(id);
		classRoom.setGrade(classRoomToUpdate.getGrade());
		classRoom.setName(classRoomToUpdate.getName());
		//classRoom.setStudents(classRoomToUpdate.getStudents());
		updateClassRoom(classRoom);
		
	}
	
	public void removeClassRoomById(long id)
	{
		ClassRoom classRoom=findClassRoomById(id);
		removeClassRoom(classRoom);
	}

}
