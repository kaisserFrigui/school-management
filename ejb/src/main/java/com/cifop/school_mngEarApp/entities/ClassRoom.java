package com.cifop.school_mngEarApp.entities;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Table;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
@Table(name = "Class")
public class ClassRoom {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	private String grade;
	private String name;
	@OneToMany(fetch = FetchType.EAGER)
	private List<Student> student;
	
	

	public ClassRoom(long id, String grade, String name, List<Student> student) {
		super();
		Id = id;
		this.grade = grade;
		this.name = name;
		this.student = student;
	}



	public ClassRoom() {
	}

	

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public List<Student> getStudent() {
		return student;
	}



	public void setStudent(List<Student> student) {
		this.student = student;
	}



}
