package com.cifop.school_mngEarApp.entities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

@Entity
@Table(name = "Student")
@NamedQuery(name = "FindAllStudents",query = "SELECT s from Student s")
@NamedQuery(name = "FindStudentByFirstNameQuery",query = "SELECT s from Student s where s.firstName Like :firstName")
//OnetoMany ManytoOne
//@NamedQuery(name="FindStudentByClassroom",query = "SELECT c from classroom c join c.students s where s.id= :idStudent")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String firstName;
	private String lastName;
	private String adress;
	private Date dateOfBirth;
	@ManyToOne
	private ClassRoom classroom;

	public Student() {

	}

	public Student(long id, String firstName, String lastName, String adress, Date dateOfBirth, ClassRoom classroom) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
		this.dateOfBirth = dateOfBirth;
		this.classroom = classroom;
	}

	public Student(long id, String firstName, String lastName, String adress, Date dateOfBirth) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
		this.dateOfBirth = dateOfBirth;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public ClassRoom getClassroom() {
		return classroom;
	}

	public void setClassroom(ClassRoom classroom) {
		this.classroom = classroom;
	}

}
